//
//  ViewController.swift
//  binary-app
//
//  Created by Nadila Dithmal on 7/16/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //IBOutlets for text field and buttons
    @IBOutlet weak var valueEntryTxtField: UITextField!
    @IBOutlet weak var binaryBtn: UIButton!
    @IBOutlet weak var decimalBtn: UIButton!
    
    let placeholder = NSAttributedString(string: "Enter a value...", attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1), NSAttributedStringKey.font: UIFont(name: "Menlo", size: 36.0)!]) //Placeholder text properties
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        valueEntryTxtField.attributedPlaceholder = placeholder //set the place holder to the properties variable
        disableButtons() //disable buttons when view loads
        valueEntryTxtField.addTarget(self, action: #selector(textFieldTextDidChange), for: .editingChanged) //calls the function everytime a change occurs in the text field
//        let binDigit = BinaryDecimal([1,0,0,0,1,1,0,1])
//        print(binDigit.calculateIntValueForBinary())
    }
    @IBAction func binaryBtnWasPressed(_ sender: Any) {
        if valueEntryTxtField.text != "" {
            binaryBtn.alpha = 1.0
            decimalBtn.alpha = 0.5
            
            guard let string = valueEntryTxtField.text, let intFromString = Int(string) else { return }
            let binaryDigit = BinaryDecimal(intFromString)
            valueEntryTxtField.text = "\(binaryDigit.calculateBinaryValueForInt())"
        }
        
    }
    
    @IBAction func decimalBtnWasPressed(_ sender: Any) {
        if valueEntryTxtField.text != "" {
            binaryBtn.alpha = 0.5
            decimalBtn.alpha = 1.0
            
            guard let string = valueEntryTxtField.text else { return }
            let bitsFromString = string.map { Int(String($0))! }
            
            let decimalValue = BinaryDecimal(bitsFromString)
            valueEntryTxtField.text = "\(decimalValue.calculateIntValueForBinary())"
        }
    }
    
    @objc func textFieldTextDidChange() {
        if valueEntryTxtField.text == "" { //if text field is empty, disable the buttons
            disableButtons()
        } else {
            enableButtons()
        }
    }
    
    func disableButtons() { //disable button function
        binaryBtn.isEnabled = false
        binaryBtn.alpha = 0.5
        decimalBtn.isEnabled = false
        decimalBtn.alpha = 0.5
    }
    
    func enableButtons(){ //enable button function
        binaryBtn.isEnabled = true
        binaryBtn.alpha = 1.0
        decimalBtn.isEnabled = true
        decimalBtn.alpha = 1.0
    }
    
}

